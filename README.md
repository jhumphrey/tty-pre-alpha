# Typewriter-pre-ALPHA

A simple typewriter app for x86
Our world is full of distractions, and so are our computers.
This aims to be a distraction free environment. SEE BUGS
## Atention!
This Software comes without any warranty!
It is pre-alpha Software, and I would recommend that you read and understand the entire source before running on real hardware.
I am not even running it on real hardware (yet)
I use a virtual machine.

## Installation
### Prerequisites:

* `nasm` (or a compatible assembler)
* `dd` or a similar piece of software
* `git` or another way to obtain the source
* `qemu` or another way to run the software. (an x86 computer should also work, but is not recommended at this phase)

### Download
``` bash
git clone https://gitlab.com/jhumphrey/tty-pre-alpha.git
```
### Build:
``` bash
nasm -f bin TTY-pre-alpha.asm
```
### Install (virtual machine):
```sh
qemu-img create tty.img 1M 
dd if=TTY-pre-alpha of=tty.img conv=notrunc
```
### Run (virtual machine):
```sh
qemu-system-i386 target.img
```
## Bug reporting:
If something does not work as expected :gasp: please file an issue.