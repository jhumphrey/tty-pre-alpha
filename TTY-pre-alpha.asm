%define BARSTART 0xb8f00 
%define TABSTOP	0x7e00	;Where is the tabstop in memory?

[bits 16]
org 0x7c00

;set mode
mov ax, 0x0003
int 10H
mov di, 500h
call init_scr
mov cx, di ;offset in the memory.
;push ecx


loop:
;get a key
xor ax,ax
int 16h
cmp al, 03h ;is it CTRL-C [ETX]
je end

;code to handle the F keys.
;3b is F1
;44h is F10
;We could do all of them, but it would be better to subtract 3b from ah, and if it is less than 10, set tabstop.
cmp ah, 3bh
jnge .aftertabcheck
cmp ah, 44h
jnle .aftertabcheck
sub ah, 3bh
mov byte [TABSTOP], ah
mov byte [ds:di], 0x1b
add di, 1
mov byte [ds:di], ah
add di, 1
.aftertabcheck:

cmp al, 0
je loop

cmp al, 0Ch	;is it FORM FEED? NEW PAGE
je newpage
mov [ds:di], al
add di, 1
;test code to add a zero at the end
mov word [ds:di], 0011h

;graphics routine
push di
mov di, cx
mov ebx, 0xb8000
.graphics:
mov ah, 0x20
mov al, [ds:di]
cmp al, 0 ;5ah ;0
je .freshair
cmp al, 0ah	;Is it a LINE FEED?
je .nl
cmp al, 0dh	;IS IT CAIRAGE RETURN
je .nl
cmp al, 09h	;IS IT TAB? 
je .ht
cmp al, 1bh	;Is it ESCAPE?
jne .otherchar ;NO, go ahead.
add di, 1
mov al, byte [ds:di]
mov byte [TABSTOP], al
add di, 1 ;DEBUG
jmp .graphics
.otherchar:
mov word [ebx], ax
add ebx, 2
add di, 1

;make sure we do not need a new page.
cmp ebx, BARSTART
jnle newpage
jmp .graphics

.nl:
push ecx
mov byte [ebx], 0x20
xor edx, edx
mov eax, ebx
sub eax, 0xb8000
mov ecx, 160 ;80 ;the width of the string
div ecx
add ax, 1
imul ax, 160 ;the width of the screen
add eax, 0xb8000

mov bl, byte [TABSTOP]
shl bl, 3	;multiply by eight
add al, bl
mov ebx, eax
add di, 1
pop ecx
jmp .graphics
.ht:
;Add 16 each character is WORD+COLOR
add ebx, 16
;And by eight
and bl, 0FAh
add di, 1 ;DEBUG
jmp .graphics
.freshair:
pop di
jmp loop
end:
xor ax,ax
mov al, 01h ;TODO DEBUG
int 10h
;pop ecx
call disk_write
hlt
ret

init_scr:
push cx
;blank screen
mov cx, 1920
mov ebx, 0xb8000
.blank:
	mov word [ebx], 0x2020
	add ebx, 2
loop .blank
mov ebx, BARSTART
mov si, helpmsg
mov cx, 80
mov ah, 17h
.bbar:
	lodsb
	mov word [ebx], ax
	add ebx, 2
;	cmp al, 0
;	jne .bbar
loop .bbar
pop cx
ret
disk_write:

;Calculate number of sectors to write
push ax
;routine to calculate file size
;how many bytes is it?
	mov ax, di
	sub ax, 500h
;Now we have the number of bytes, how many sectors is that? 
	shr ax, 9	;I believe this is equivalent to divide ax, 512 but faster
	add ax, 1	;example: if the file is one byte, and it is shifted right by nine, then that is zero sectors, but we need one, hence the add.

mov si, disk
.getDisk:
lodsb
cmp al, 0
mov ah, 0Eh
int 10H
jne .getDisk

xor ax, ax
int 16H
sub al, 31h
add al, 80h
mov dl, al
mov ah, 0Eh
add al, 30h
int 10H
mov al, 0Dh
int 10H

mov si, sector
.getSec:
lodsb
cmp al, 0
mov ah, 0Eh
int 10H
jne .getSec

xor ax, ax
int 16H
sub al, 30h
mov cl, al

add al, 30h
mov ah, 0Eh
int 10H

;Ah, now we have the number of sectors in al! that is good as long as we are >=	 255 sectors!
mov ebx, 500h	;the calculated offset
;mov dl, 80h	;the first hard drive. TODO: use a macro.
mov ah, 03h	;DISK_WRITE
mov ch, 0	;TRACK
;mov cl, 2	;SECTOR
mov dh, 0	;HEAD
;DL is drive
;AL is number of sectors.
int 13h
;push cx
;xor cx, cx
;mov cl, al
;.debugB:
;mov al, 66
;mov ah, 0Eh
;int 10H
;loop .debugB

;pop cx
;cmp al, 3
;pop ax
;je loop
ret

newpage:
;pop ecx
;call disk_write
mov byte [ds:di], 0ch
add di, 1
mov cx, di
call init_scr
;push ecx
jmp loop

ret


helpmsg: db "TYPEWRITER alpha |      [C]LOSE        [L]OAD PAGE                    <(:):(:)>",0
disk: db "DISK: ",0
sector: db "SECTOR: ",0
;end: "Save complete",0
times 510-($-$$) db 0
dw 0xAA55
times 512 db 0

;	7E00 MEMORY MAP
;	TABSTOP POSITION	7E00	1 BYTE
;	PAGE ADDRESSES		
;	PAGE 1, 		7E01	1 WORD
