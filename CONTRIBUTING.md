# Some guidlines for contributing

* Comment your code, If in doubt, comment.
* If it ain't broke, don't fix it.
* All code is MIT licensed.
* These guidelines may be ignored by me (jhumphrey)
* These guidlines are subject to change without notice.